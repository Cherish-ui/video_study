﻿#include <iostream>
//一个基于TCP的RTSP服务器，加入协议的处理
//一个基于UDP的RTP服务器，加入数据的处理
#include"RTSPServer.h"

int main()
{
    //1.分配给m_thread一个threadWorker()的任务
    RTSPServer server;
    server.Init();
    //启动该线程执行任务(这个任务是给线程池分配一个ThreadSession任务)会不断检查执行,并启动线程池,线程池获取到这个任务之后会让各个线程不断遍历自身的任务,如果有任务就执行
    server.Invoke();
    std::cout << "prepare to exit!" << std::endl;
    getchar();
    server.Stop();
    return 0;
}


