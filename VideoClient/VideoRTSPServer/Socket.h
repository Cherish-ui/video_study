#pragma once
#pragma warning(disable:6031)
#include<WinSock2.h>
#include <iostream>
#include<memory>
#include<string.h>
#include"base.h"
using namespace std;
#pragma warning(disable:6031)
#pragma comment(lib,"ws2_32.lib")
class Socket
{
public:
	//nType 0 Tcp 1 UDP
	Socket(bool bIsTcp = true)
	{
		m_sock = INVALID_SOCKET;
		if (bIsTcp)
		{
			m_sock = socket(PF_INET, SOCK_STREAM, 0);
		}
		else
		{
			m_sock = socket(PF_INET, SOCK_DGRAM, 0);
		}
	}
	Socket(SOCKET &s)
	{
		m_sock = s;
	}
	void Close()
	{
		if (m_sock != INVALID_SOCKET)
		{
			SOCKET temp = m_sock;
			m_sock = NULL;
			closesocket(temp);
		}
	}
	operator SOCKET()
	{
		return m_sock;
	}
	~Socket() {
		Close();
	};
	SOCKET GetSock() { return m_sock; };
private:
	SOCKET m_sock;
};
//存放数据类
class EAddress
{
public:
	EAddress()
	{
		m_port = -1;
		memset(&m_addr, 0, sizeof(m_addr));
		m_addr.sin_family = AF_INET;
	};
	EAddress(const std::string& ip, short port)
	{
		m_ip = ip;
		m_port = port;
		m_addr.sin_port = htons(port);
		m_addr.sin_addr.s_addr = inet_addr(ip.c_str());
	};
	EAddress(const EAddress& addr)
	{
		m_ip = addr.m_ip;
		m_port = addr.m_port;
		memcpy(&m_addr, &addr.m_addr, sizeof(sockaddr_in));
	};
	EAddress& operator=(const EAddress& addr)
	{
		if (this != &addr)
		{
			m_ip = addr.m_ip;
			m_port = addr.m_port;
			memcpy(&m_addr, &addr.m_addr, sizeof(sockaddr_in));
		}
		return *this;
	};
	EAddress& operator=(short port)
	{
		m_port =(unsigned short) port;
		m_addr.sin_port = htons(port);
		return*this;
	}
	~EAddress() {};
	void Updata(const std::string& ip, short port)
	{
		m_ip = ip;
		m_port = port;
		m_addr.sin_port = htons(port);
		m_addr.sin_addr.s_addr = inet_addr(ip.c_str());
	};
	//类型转换
	operator sockaddr* ()const
	{
		return (sockaddr*)&m_addr;
	};
	operator sockaddr* ()
	{
		return (sockaddr*)&m_addr;
	};
	operator sockaddr_in* ()
	{
		return &m_addr;
	};
	int size()const
	{
		return sizeof(sockaddr_in);
	}
	const std::string Ip()const
	{
		return m_ip;
	}
	unsigned short Port()const
	{
		return m_port;
	}
private:
	std::string m_ip;
	unsigned m_port;
	sockaddr_in m_addr;
};
class ESocket
{
public:
	ESocket(bool isTcp = true)
		:m_socket(new Socket(isTcp)),m_istcp(true){};
	ESocket(const ESocket& sock) :m_socket(sock.m_socket),m_istcp(sock.m_istcp){}
	ESocket& operator=(const ESocket& sock)
	{
		if (this != &sock)m_socket = sock.m_socket;
		return *this;
	}
	ESocket(SOCKET sock, bool isTcp) :
		m_socket(new Socket(sock)),
		m_istcp(isTcp) {};
	~ESocket()
	{
		m_socket.reset();
	}
	operator SOCKET const()
	{
		return *m_socket;
	}
	int Bind(const EAddress& addr)
	{
		if (m_socket == nullptr)
		{
			m_socket.reset(new Socket(m_istcp));//如果原有的shared_ptr不为空，会使原对象的引用计数减1
		}
		return bind(*m_socket, addr, addr.size());
	}
	int Listen(int backlog = 5)
	{
		return listen(*m_socket, backlog);
	}
	ESocket Accept(EAddress& addr)
	{
		int len = addr.size();
		if (m_socket == nullptr)return ESocket(INVALID_SOCKET,true);
		SOCKET server = *m_socket;
		if (server == INVALID_SOCKET)return ESocket(INVALID_SOCKET, true);
		SOCKET s= accept(server, addr, &len);
		return ESocket(s, m_istcp);
	}
	int Connect(const EAddress& addr)
	{
		return connect(*m_socket,addr,addr.size());
	}
	int Recv(EBuffer& buffer)
	{
		return recv(m_socket.get()->GetSock(), buffer, buffer.size(), 0);
	}
	int Send(const EBuffer& buffer)
	{
		printf("send %s\r\n", (char*)buffer);
		/*return send(*m_socket, buffer, buffer.size(), 0);*/
		int index = 0;
		char* pData = buffer;
		while (index<buffer.size())
		{
			int ret= send(*m_socket, pData+index, buffer.size()-index, 0);
			if (ret < 0)return ret;
			if (ret == 0)break;
			index += ret;
		}
		return index;
	}
	void Close()
	{
		m_socket.reset();
	}
private:
	std::shared_ptr<Socket>m_socket;
	bool m_istcp;
};
//当你执行SOCKET sock = mySocket;时，会首先调用ESocket::operator SOCKET()，然后在这个函数内部会调用Socket::operator SOCKET()
class SocketIniter
{
public:

	SocketIniter()
	{
		WSADATA wsa;
		WSAStartup(MAKEWORD(2, 2), &wsa);
	}
	~SocketIniter()
	{
		WSACleanup();
	}
};
//记录SOCK的引用次数,不到最后一个SOCK销毁,系统不执行SOCK回收,不希望由于SOCK赋值导致被意外关闭

//思路1.:建立一个静态的map存入SOCK与count,count为零再析构SOCK
//思路2.使用shared_ptr,对其封装使其表现得不那么像一个指针