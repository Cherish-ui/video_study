#pragma once
#include"base.h"
class MideaFile
{
public:
	MideaFile() {};
	~MideaFile();
	int Open(const EBuffer& path,int nType=96);
	//如果buffer的size为0,则表示没有帧了
	EBuffer ReadOneFrame();
	void Close();
	//重置后,ReadOneFream又会有值返回
	void Reset();
private:
	//为以后扩展做好基础
	EBuffer ReadH264Frame();
	//返回-1表明查找失败
	long FindH264Head(int& headsize);
private:
	long m_size;//确定文件的结尾
	FILE* m_file;
	EBuffer m_filepath;
	//96 H264
	int m_type;
};